export const ADD_MSG = 'ADD_MSG';
export const PARSE_MSGS = 'PARSE_MSGS';
import fix2json from '../utils/fix2json';
import { normalize, Schema, arrayOf, valuesOf } from 'normalizr';


export function parseMessage(text) {
  return dispatch => dispatch({
    type: PARSE_MSGS,
    payload : fix2json.buildFromMessageList(text.split('\n'),'|')
  })
}

export function addMessage(message) {
  return dispatch => dispatch({
    type: ADD_MSG,
    payload : message
  })
}
