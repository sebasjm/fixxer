export const FIELD_ON = 'FIELD_ON';
export const FIELD_OFF = 'FIELD_OFF';
export const FIELD_TOGGLE = 'FIELD_TOGGLE';

export function showField(field) {
  return dispatch => dispatch({
    type: FIELD_ON,
    payload : field
  })
}
export function hideField(field) {
  return dispatch => dispatch({
    type: FIELD_OFF,
    payload : field
  })
}
export function toggleField(field) {
  return dispatch => dispatch({
    type: FIELD_TOGGLE,
    payload : field
  })
}
