
var fs = require('fs');
var xpath = require('xpath');
var _ = require('underscore');
var DOMParser = require('xmldom').DOMParser;
var readline = require('readline');
var StringDecoder = require('string_decoder').StringDecoder;
var decoder = new StringDecoder();
var delim = String.fromCharCode('\x01'); // ASCII start-of-header
var dictname;
var filename;
var TAGS = {};
var GROUPS = {};
var FIX_VER = undefined;
var rd = {};
// some of these TODO's below are very speculative:
//
// TODO: decouple logic from file ingestion, but if this was a browser module, how would we package the dictionaries?
// TODO: get dictionary management out of this module
// TODO: XML merge for customizing data dictionaries with fragments
// TODO: ability to hold multiple dictionaries in memory
// TODO: autodetect FIX version from source data?
// TODO: option to flatten groups?
// TODO: emit pre and post processing events for each message processed
// TODO: forward engineer JSON to FIX?  Would be pretty useful for browser UI's based on FIX dictionaries

//checkParams();

// establish Xpaths for the requisite data dictionary versions
var groupXPath = {};
groupXPath['5.0.2'] = '//fix/components/component/group';
groupXPath['5.0.1'] = '//fix/components/component/group';
groupXPath['5.0.0'] = '//fix/components/component/group';
groupXPath['4.2.0'] = '//fix/messages/message/group';;
groupXPath['4.4.0'] = '//fix/messages/message/group';
groupXPath['1.1.0'] = '//fix/messages/message/group';

function parseFix(filename) {
	try {
		console.log("opening ", filename)
		var input = filename ? fs.createReadStream(filename) : process.stdin;
		console.log("input ", input)
		rd = readline.createInterface({
			input: input,
			output: process.stdout,
			terminal: false
		});

		console.log("read line ddd")
		process.stdin.on('data',function(){
			console.log("dale")
		})
		rd.on('line', function(line) {
			console.log("read line ", line)
		  		if (line.indexOf(delim) > -1) {
					console.log(decoder.write(processLine(line)));
				}
		});
	} catch(mainException) {
		console.error("Error in main routine: " + mainException);
	}
}

function pluckGroup(tagArray, groupName) {
	var group = [];
	var member = {};
	var firstProp = undefined;
	var idx = 0;

	while (tagArray.length > 0) {
		var tag = tagArray.shift();
		var key = tag.tag;
		var val = tag.val;
		if (idx === 0) {
			firstProp = key;
			member[key] = val;
		} else if (_.contains(Object.keys(GROUPS), key)) {
			member[key] = val;
			var newGroup = pluckGroup(tagArray, key);
		 	member[key.substring('No'.length)] = newGroup;
		} else if (key === firstProp && idx > 0) {
			group.push(member);
			member = {};
			member[key] = val;
		} else if (!_.contains(GROUPS[groupName], key)) {
			tagArray.push(tag)
			group.push(member);
 			return group;
		} else {
			member[key] = val;
		}
		idx++;
	}

}

function resolveFields(fieldArray) {
	var targetObj = {};
	var group = [];
	while (fieldArray.length > 0) {
		var field = fieldArray.shift();
		var key = field.tag;
		var val = field.val;
		if (_.contains(Object.keys(GROUPS), key)) {
			targetObj[key] = val;
			var newGroup = pluckGroup(fieldArray, key);
		 	targetObj[key.substring('No'.length)] = newGroup;
		} else {
			targetObj[key] = val;
		}
	}
	return targetObj;
}

function readLine(line, delimiter = delim) {
	return resolveFields(extractFields(line, delimiter));
}

function buildFromMessage(msg, id, delimiter = delim) {
	return msg === "" ? undefined : {
		id,
		checksum : msg,
		...readLine(msg, delimiter)
	}
}

function buildFromMessageList(msgList, delimiter = delim) {
	let i = 0
	return msgList.map( (msg) => buildFromMessage(msg, i++, delimiter) ).filter(e=>e)
}

function processLine(line) {
	return JSON.stringify(readLine(line));
}

function extractFields(record, delimiter) {
	var fieldArray = [];
	var fields = record.split(delimiter);

	for (var i = 0; i < fields.length; i++) {
    var both = fields[i].split('=');
		both[0].replace("\n", '').replace("\r", '');
		if (both[1] !== undefined) {
			//var tag = TAGS[both[0]] ? TAGS[both[0]].name : both[0];
			//var val = mnemonify(both[0], both[1]);
			var tag = both[0]
			var val = both[1]
			fieldArray.push({
				tag: tag,
				val: val
			});
		}
	}
	return fieldArray;
}

function mnemonify(tag, val) {
	return TAGS[tag] ? (TAGS[tag].values ? (TAGS[tag].values[val] ? TAGS[tag].values[val] : val) : val) : val;
}

function parseDictionary(fileLocation) {
	var xml = fs.readFileSync(fileLocation).toString();
	var dom = new DOMParser().parseFromString(xml);
	var nodes = xpath.select("//fix/fields/field", dom);

	getFixVer(dom);

	for (var i = 0; i < nodes.length; i++) {
		var tagNumber = nodes[i].attributes[0].value
		var tagName = nodes[i].attributes[1].value;
		var tagType = nodes[i].attributes[2].value;
		var valElem = nodes[i].getElementsByTagName('value');
		var values = {};

		for (var j = 0; j < valElem.length; j++) {
			values[valElem[j].attributes[0].value] = valElem[j].attributes[1].value.replace(/_/g, ' ');
		}

		TAGS[tagNumber] = {
			id : tagNumber,
			name: tagName,
			values: values,
			type: tagType,
		};
	}
	dictionaryGroups(dom);
}

function getFixVer(dom) {
	var fixMaj = xpath.select("//fix/@major", dom)[0].value;
	var fixMin = xpath.select("//fix/@minor", dom)[0].value;
	var fixSp = xpath.select("//fix/@servicepack", dom)[0].value;
   	FIX_VER = [fixMaj, fixMin, fixSp].join('.');
}

function dictionaryGroups(dom) {
	var groupNodes = xpath.select(groupXPath[FIX_VER], dom);
	for (var i = 0; i < groupNodes.length; i++) {
		var groupName = groupNodes[i].attributes[0].value;
		GROUPS[groupName] = [];
		var fields = groupNodes[i].getElementsByTagName('field');
		for (var j = 0; j < fields.length; j++) {
			var attr = fields[j].attributes[0].value;
			GROUPS[groupName].push(attr);
		}
	}
}

export default { parseFix, parseDictionary, processLine, readLine, TAGS, mnemonify, buildFromMessageList, buildFromMessage }
