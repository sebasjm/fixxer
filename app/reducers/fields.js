import { FIELD_ON, FIELD_OFF, FIELD_TOGGLE } from '../actions/fields';

const initial = [];

[8,9,10,34].forEach((e)=>{
  initial[e] = true
});

export default function fields(state = initial, action) {
  const result = []
  state.forEach((e,i)=>{
    result[i] = true
  })
  switch (action.type) {
    case FIELD_ON:
      delete result[action.payload]
      break;
    case FIELD_OFF:
      result[action.payload] = true
      break;
    case FIELD_TOGGLE:
      result[action.payload] = !state[action.payload];
      break;
    default:
      return state;
  }
  return result
}
