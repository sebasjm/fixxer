import { combineReducers } from 'redux'
import fixlog from './fixlog'
import fields from './fields'

const rootReducer = combineReducers({
  fixlog,
  fields
});

export default rootReducer;
