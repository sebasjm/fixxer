import React, { Component, PropTypes } from 'react';
import fix2json from '../utils/fix2json'
import Perf from 'react-addons-perf';
export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render() {
    window.Perf = Perf
    return (
      <div>
        <div style={{width : process.env.NODE_ENV !== 'production' ? "70%" : "100%"}}>
          {this.props.children}
      </div>
        {
          (() => {
            if (process.env.NODE_ENV !== 'production') {
              const DevTools = require('./DevTools');
              return <DevTools />;
            }
          })()
        }
      </div>
    );
  }
}
