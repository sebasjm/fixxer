import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FixLog from '../components/FixLog';
import * as fixlogActions from '../actions/fixlog';
import * as fieldActions from '../actions/fields';

function mapStateToProps(state) {
  return {
    fixlog: state.fixlog,
    fields: state.fields,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...fieldActions, ...fixlogActions}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FixLog);
