import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './FixLog.module.css';
import fix2json from '../utils/fix2json';
import LogField from './LogField'

class LogTable extends Component {

  static propTypes = {
    message: PropTypes.string.isRequired,
    hideField: PropTypes.func.isRequired,
  };

  render() {
    const { hideField, hiddenFields, message, messageId } = this.props
    const allFields = !message ? [] : Object.keys(message);
    return (
      <div className={styles.message}>
        <table  className={messageId % 2 ? "" : "pull-right"}>
          <tbody>
            { allFields.map(
              (fieldId) => (<LogField key={messageId+'_'+fieldId} hideField={hideField} hiddenFields={hiddenFields} fieldId={fieldId} msg={message[fieldId]} />)
            ) }
          </tbody>
        </table>
        <div style={{clear : 'both'}} />
      </div>
    )}
}

export default LogTable;
