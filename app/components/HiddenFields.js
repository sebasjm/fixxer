import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './FixLog.module.css';
import fix2json from '../utils/fix2json';
import { Label, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import LogTable from './LogTable'

class HiddenFields extends Component {
  render() {
    const { fields, showField } = this.props
    return (
      <div>
        {fields.map((elem,field)=> fields[field] ? field : undefined).filter(f=>!!f).map(field=>
          <div key={field} style={{display: 'inline', float: 'left'}}>
            <Label onClick={()=>showField(field)}>+</Label>
            <Label>
              <a href={"http://www.onixs.biz/fix-dictionary/4.2/tagNum_"+field+".html"} target="_blank">
                { fix2json.TAGS[field] ? fix2json.TAGS[field].name : field }
              </a>
            </Label>
        </div>
        )}
        <div style={{clear : 'both'}} />
      </div>
    )}
}

export default HiddenFields
