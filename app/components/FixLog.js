import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './FixLog.module.css';
import fix2json from '../utils/fix2json';
import { Label, Button, Input, OverlayTrigger, Tooltip } from 'react-bootstrap'
import LogTable from './LogTable'
import HiddenFields from './HiddenFields'
import MessageInput from './MessageInput'

class FixLog extends Component {
  static propTypes = {
    fixlog: PropTypes.array.isRequired,
    showField: PropTypes.func.isRequired,
    hideField: PropTypes.func.isRequired,
  };

  componentWillMount() {
    fix2json.parseDictionary("app/utils/dict/FIX50.xml")
  }



  render() {
    const { fixlog, fields } = this.props
    const { showField, hideField, parseMessage } = this.props

    let messageId = 0;

    return (
      <div>
        <div className={styles.backButton}>
          <Link to="/">
            <i className="fa fa-arrow-left fa-3x" />
          </Link>
        </div>
        <MessageInput parseMessage={parseMessage}/>
          <Label>{fixlog.length + ' messages'}</Label>
        <hr/>
        <HiddenFields showField={showField} fields={fields}/>
        { fixlog.map(
            (msg) => (<LogTable key={messageId} messageId={messageId++} hideField={hideField} hiddenFields={fields} message={msg}/>)
          )
        }
      </div>
    );
  }
}

export default FixLog;
