import React, { Component, PropTypes } from 'react';
import { Label, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import debounce from '../utils/debounce'
const example = [
  "8=FIXT.1.1|9=167|35=V|34=8|49=Glomat01|52=20160118-20:54:07.918|56=ROFX|115=Glomat01|128=ROFX|262=NAD2|263=1|264=5|265=0|266=N|7118=D|146=1|55=C_AUT_Test_01|207=ROFX|267=2|269=0|269=1|10=001|",
  "8=FIXT.1.1|9=127|35=W|34=40|49=ROFX|52=20160118-20:54:32.649|56=Glomat01|55=C_AUT_Test_01|207=ROFX|262=NAD2|264=5|268=2|269=0|290=0|269=1|290=0|10=238|",
  "8=FIXT.1.1|9=205|35=D|34=10|49=Glomat01|52=20160118-20:54:12.617|56=ROFX|115=Glomat01|128=ROFX|1=10|11=56843b74e42c319700adc72dbf77c6ff|18=x |38=10|40=2|44=10.0|54=1|55=C_AUT_Test_01|59=0|60=20160118-20:54:12.617|207=ROFX|10=148|",
  "8=FIXT.1.1|9=160|35=W|34=46|49=ROFX|52=20160118-20:54:37.437|56=Glomat01|55=C_AUT_Test_01|207=ROFX|262=NAD2|264=5|268=2|269=0|270=10.0|271=10|288=I_AuAg1|1=10|290=1|269=1|290=0|10=168|",
  "8=FIXT.1.1|9=341|35=W|34=47|49=ROFX|52=20160118-20:54:37.443|56=Glomat01|55=C_AUT_Test_01|207=ROFX|262=TOP1|264=1|268=9|269=0|270=10.0|271=10|288=I_AuAg1|1=10|290=1|7202=10|269=1|290=0|269=2|270=18.0|271=35|272=20160118|273=16:56:55.578|277=U|7201=1|269=4|270=18.000000|269=7|270=18.000000|269=8|270=17.0|269=B|271=135.000000|269=x|271=4050|269=w|270=7200.0|10=123|",
  "8=FIXT.1.1|9=127|35=W|34=49|49=ROFX|52=20160118-20:54:51.125|56=Glomat01|55=C_AUT_Test_01|207=ROFX|262=NAD2|264=5|268=2|269=0|290=0|269=1|290=0|10=237|",
  "8=FIXT.1.1|9=300|35=W|34=50|49=ROFX|52=20160118-20:54:51.138|56=Glomat01|55=C_AUT_Test_01|207=ROFX|262=TOP1|264=1|268=9|269=0|290=0|269=1|290=0|269=2|270=18.0|271=35|272=20160118|273=16:56:55.578|277=U|7201=1|269=4|270=18.000000|269=7|270=18.000000|269=8|270=17.0|269=B|271=135.000000|269=x|271=4050|269=w|270=7200.0|10=076|",
]

class MessageInput extends Component {

  render() {

    const send = () => {
      this.props.parseMessage(this.refs.fixmsg.value)
    }

    return (
      <div>
        <Button onClick={()=>{
          this.refs.fixmsg.value= example.join('\n')
          send()
        }}>Load Example</Button>
        <textarea rows="4" ref="fixmsg" style={{width:"100%",color:'black'}} onChange={debounce(send,500)}/>
      </div>
    );
  }

}

export default MessageInput
