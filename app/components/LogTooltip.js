import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './FixLog.module.css';
import fix2json from '../utils/fix2json';
import { Label, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'


class LogTooltip extends Component {

  render () {
    const {msgKey, msgValue} = this.props
    const type = fix2json.TAGS[msgKey] ? fix2json.TAGS[msgKey].type : msgKey;
    const typeTitle = !!type?'  ('+type+')':''
    const tooltip = (
          <Tooltip id={"tooltipId"+msgKey}>
            <strong>{msgKey+'='+msgValue}</strong>{typeTitle}
          </Tooltip>
        );
    return (
      <OverlayTrigger placement="top" overlay={tooltip}>
        {this.props.children}
      </OverlayTrigger>
    )}
}


export default LogTooltip;
