import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './FixLog.module.css';
import fix2json from '../utils/fix2json';
import { Label, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import LogTooltip from './LogTooltip'

class LogField extends Component {

    render() {
      const { hideField, hiddenFields, fieldId, msg } = this.props
      if (fieldId === 'checksum') return <tr/>
      if (fieldId === 'id') return <tr/>
      return (
        <tr key={fieldId} className="required-field header-field" style={!!hiddenFields[fieldId] ? {display: "none"} : undefined}>
            <td className={styles.right}>

              <LogTooltip msgKey={fieldId} msgValue={msg} >
                <span>
                  <a href={"http://www.onixs.biz/fix-dictionary/4.2/tagNum_"+fieldId+".html"} target="_blank">
                    <Label>
                      { fix2json.TAGS[fieldId] ? fix2json.TAGS[fieldId].name : fieldId }
                    </Label>
                  </a>
                  <a>
                    <Label onClick={()=>hideField(fieldId)}>
                    -
                  </Label>
                  </a>

                </span>
              </LogTooltip>
            </td>
            <td className={styles.left}>{fix2json.mnemonify(fieldId,msg)}</td>
        </tr>
      )}

}

export default LogField
